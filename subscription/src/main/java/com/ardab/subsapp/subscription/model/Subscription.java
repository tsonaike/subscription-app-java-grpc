package com.ardab.subsapp.subscription.model;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Data
@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int packageId;
    private String subscriberMsisdn;
    private int subscriptionStatus;

    public Subscription(int packageId, String subscriberMsisdn, int subscriptionStatus) {
        this.packageId = packageId;
        this.subscriberMsisdn = subscriberMsisdn;
        this.subscriptionStatus = subscriptionStatus;
    }
}
