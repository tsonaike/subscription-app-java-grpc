package com.ardab.subsapp.subscription.exception;

import com.ardab.gprc.subscription.SubscriptionErrorCode;
import lombok.Getter;

@Getter
public class SubscriptionException extends RuntimeException{

    private SubscriptionErrorCode errorCode;

    public SubscriptionException(SubscriptionErrorCode errorCode){
        super(errorCode.name());
        this.errorCode = errorCode;
    }
}
