package com.ardab.subsapp.subscriber.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

@Data
public class SubscriptionCreateRequest {
    private int packageId;

    public SubscriptionCreateRequest() {
    }

    public SubscriptionCreateRequest(int packageId) {
        this.packageId = packageId;
    }

}
