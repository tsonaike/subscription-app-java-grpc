package com.ardab.subsapp.subscriber.model;

import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

@Data
public class SubscriberCreateRequest {

    private String subscriberMsisdn;
    private String subscriberName;
    private String subscriberSurname;
    private double subscriberBalance;

    public SubscriberCreateRequest(String subscriberMsisdn, String subscriberName, String subscriberSurname, double subscriberBalance) {
        this.subscriberMsisdn = subscriberMsisdn;
        this.subscriberName = subscriberName;
        this.subscriberSurname = subscriberSurname;
        this.subscriberBalance = subscriberBalance;
    }
}

