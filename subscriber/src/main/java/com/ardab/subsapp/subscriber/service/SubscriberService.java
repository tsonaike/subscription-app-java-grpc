package com.ardab.subsapp.subscriber.service;

import com.ardab.subsapp.subscriber.model.Subscriber;
import com.ardab.subsapp.subscriber.model.SubscriberCreateRequest;
import com.ardab.subsapp.subscriber.model.SubscriptionCreateResponse;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SubscriberService {

    public ResponseEntity<List<Subscriber>> getAllSubscriber();
    public ResponseEntity<Subscriber> getSubscriber(Long id);
    public ResponseEntity<Subscriber> createNewSubscriber(SubscriberCreateRequest subscriber, HttpServletRequest request);
    public ResponseEntity<Subscriber> updateSubscriber(Long id, SubscriberCreateRequest subscriber);
    public ResponseEntity<Subscriber> deleteSubscriber(Long id);
    public ResponseEntity<SubscriptionCreateResponse> createSubscription(Long id, int packageId);
}
