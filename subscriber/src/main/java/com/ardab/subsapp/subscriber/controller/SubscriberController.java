package com.ardab.subsapp.subscriber.controller;

import com.ardab.subsapp.subscriber.model.Subscriber;
import com.ardab.subsapp.subscriber.model.SubscriberCreateRequest;
import com.ardab.subsapp.subscriber.model.SubscriptionCreateRequest;
import com.ardab.subsapp.subscriber.model.SubscriptionCreateResponse;
import com.ardab.subsapp.subscriber.service.SubscriberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/v1/subscribers")
public class SubscriberController {

    @Autowired
    SubscriberServiceImpl subscriberServiceImpl;

    @PostMapping("/create")
    public ResponseEntity<Subscriber> createNewSubscriber(@RequestBody SubscriberCreateRequest subscriber, HttpServletRequest request){
         return subscriberServiceImpl.createNewSubscriber(subscriber, request);
    }

    @PostMapping("/{id}/createsubscription")
    public ResponseEntity<SubscriptionCreateResponse> createSubscription(@PathVariable Long id, @RequestBody SubscriptionCreateRequest request){
        return subscriberServiceImpl.createSubscription(id,request.getPackageId());
    }

    @GetMapping
    public ResponseEntity<List<Subscriber>> getAllSubscribers(){
        return subscriberServiceImpl.getAllSubscriber();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subscriber> getOneSubscriber(@PathVariable Long id){
        return subscriberServiceImpl.getSubscriber(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Subscriber> updateSubscriber(@PathVariable Long id, @RequestBody SubscriberCreateRequest subscriber){
        return subscriberServiceImpl.updateSubscriber(id,subscriber);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Subscriber> deleteSubscriber(@PathVariable Long id){
        return subscriberServiceImpl.deleteSubscriber(id);
    }
}
