package com.ardab.subsapp.subscriber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Subscriber Not Found")
public class SubscriberNotFoundException extends RuntimeException{
}
