package com.ardab.subsapp.mobilepackage.exception;

import com.ardab.gprc.mobilepackage.MobilePackageErrorCode;
import com.ardab.gprc.mobilepackage.PackageExceptionResponse;
import com.ardab.gprc.subscription.SubscriptionExceptionResponse;
import com.google.protobuf.Any;
import com.google.protobuf.Timestamp;
import com.google.rpc.Code;
import com.google.rpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;

import java.time.Instant;

@GrpcAdvice
public class PackageExceptionHandler {

    @GrpcExceptionHandler(PackageException.class)
    public StatusRuntimeException inactivePackageError(PackageException exception){
        Instant time = Instant.now();
        Timestamp timestamp = Timestamp.newBuilder().setSeconds(time.getEpochSecond())
                .setNanos(time.getNano()).build();

        PackageExceptionResponse packageExceptionResponse = PackageExceptionResponse.newBuilder()
                .setErrorCode(exception.getMobilePackageErrorCode())
                .setTimestamp(timestamp)
                .build();
        if(packageExceptionResponse.getErrorCode() == MobilePackageErrorCode.NOT_ACTIVE_PACKAGE) {
            Status status = Status.newBuilder()
                    .setCode(Code.INVALID_ARGUMENT.getNumber())
                    .setMessage("Package not active!")
                    .addDetails(Any.pack(packageExceptionResponse))
                    .build();
            return StatusProto.toStatusRuntimeException(status);
        }
        else if(packageExceptionResponse.getErrorCode() == MobilePackageErrorCode.INVALID_PACKAGE_CODE_VALUE){
            Status status = Status.newBuilder()
                    .setCode(Code.INVALID_ARGUMENT.getNumber())
                    .setMessage("Package not exist!")
                    .addDetails(Any.pack(packageExceptionResponse))
                    .build();
            return StatusProto.toStatusRuntimeException(status);
        }
        return null;
    }
}
